
import java.io.*;
class InputFile {
	public static void main(String[] args) 
	throws IOException
	{
		String input = "C:\\Users\\Natalia\\workspace\\input.txt";
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(input), "UTF-8"));
		String str = in.readLine();
		int lineNumbers = Integer.parseInt (str);
		int [] array = new int[lineNumbers];
		for (int i = 0; i < lineNumbers; i++){
			array[i] =  Integer.parseInt(in.readLine());
			System.out.print(array[i]+ " ");
		}
		in.close();
		System.out.println();
		for (int i = array.length-1; i > 0; i--){
			int max = array [i];
			int imax = i;
			for (int j = 0; j < i; j++){
				if (array[j] > max) {
					max = array[j];
					imax = j;
				}
			}
			if (i != imax){
				int item = array[i];
				array[i] = array[imax];
				array[imax] = item;
			}
		}
		for (int i = 0; i<array.length; i++){
			System.out.print(array[i]+ " ");
		}
		System.out.println();
		
		writeToFile(array);
	}
	private static void writeToFile (int [] args) 
	throws IOException
	{
		String output = "C:\\Users\\Natalia\\workspace\\outnput.txt";
		BufferedWriter bw = new BufferedWriter(new FileWriter(output));
		String str = null;
		for (int i = 0; i < args.length; i++){
			if (i == 0){
				str = 	"" + args [i];
			} else {
				str = " " + args [i];
			}
			bw.write(str);
		}
		bw.close();
	}
}


